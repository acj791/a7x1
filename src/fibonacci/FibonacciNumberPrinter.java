package fibonacci;

import java.math.BigInteger;
import java.util.ArrayList;


public class FibonacciNumberPrinter {
    
    ArrayList<BigInteger> fibNumber = new ArrayList<BigInteger>();
    
    int count = 1;

    public void printFirstFibonacciNumbers( final int maxIndex ) {
        
        BigInteger f0 = new BigInteger("0");
        BigInteger f1 = new BigInteger("1");
        BigInteger f2 = new BigInteger("1");
        
        if( maxIndex > 0 ) {
            if( maxIndex == 1) {
                fibNumber.add(f1);
            }
            else if( maxIndex == 2) {
                fibNumber.add(f1);
                fibNumber.add(f2);
            }
            else {
                fibNumber.add(f1);
                fibNumber.add(f2);
                
                    for( int i = 1; i <= (maxIndex - 2); i++) {
                         
                            f0 = f1.add(f2);
                            f1 = f2;
                            f2 = f0;
                            fibNumber.add(f0);
                     }
            }
        }else {
            System.out.printf("%s", "Keine Gültige Eingabe"); 
        }
        
        for ( BigInteger a : fibNumber ) {
            System.out.printf("%d,", a);
        }
        
    }
    
    
    public void computeFibonacciNumber( int index ) {
        
        assert index > 0 : "Kein gültiger Parameter";
        
        if( index == 1 ) {
            System.out.printf("%d", fibNumber.get(0));
        }else {
            System.out.printf("%d", fibNumber.get(index - 1 ));
        }
        
    }
    
    
}
